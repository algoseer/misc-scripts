
if [ $# -eq 0 ]
then 
	echo "Enter a search query"
	read q
else
	q=$1
fi

tmpdir=/tmp
query=`echo $q | sed 's/ /+/g'`;

url="http://www.youtube.com/results?search_query"

echo $url=$query

curl -q $url=$query -o $tmpdir/results.html

#grep data-context-item $tmpdir/results.html   | sed 's/data/\n&/g' | grep time | grep -v __ | cut -f2 -d\" > $tmpdir/times.txt
#grep data-context-item $tmpdir/results.html   | sed 's/data/\n&/g' | grep "item-id" | grep -v __ | cut -f2 -d\" > $tmpdir/ids.txt
#grep data-context-item $tmpdir/results.html   | sed 's/data/\n&/g' | grep "item-views" | grep -v __ | cut -f2 -d\" > $tmpdir/views.txt
#grep data-context-item $tmpdir/results.html   | sed 's/data/\n&/g' | grep title | grep -v __ | cut -f2 -d\" | paste - $tmpdir/times.txt -d'(' | sed 's/$/)/g' | paste -  $tmpdir/views.txt | sed 's/$//g' | nl

grep "^  *href" $tmpdir/results.html  | grep v=   | cut -f3 -d= | sed 's/"//g' > $tmpdir/ids.txt
N=`cat $tmpdir/ids.txt | wc -l`
sed 's/>/&\n/g' $tmpdir/results.html | grep views.*li | grep -v __ | cut -f1 -d\< > $tmpdir/views.txt
grep video-time $tmpdir/results.html | grep -v __ | sed 's/[^0-9:]//g' > $tmpdir/times.txt
grep "^ *title" $tmpdir/results.html | grep -v __ | cut -f2 -d= | sed 's/"//g'| tail -n"$N" | paste - $tmpdir/times.txt -d'(' | sed 's/$/)/g' | paste -  $tmpdir/views.txt | sed 's/$//g' | nl

echo 
echo 
echo "Enter number to play"
read choice

option=`echo $choice | sed 's/[^0-9]//g'`
other=`echo $choice | sed 's/[0-9]//g'`

id=`sed -n "$option"p $tmpdir/ids.txt`
echo "Playing http://www.youtube.com/watch?v=$id"
#rm $tmpdir/results.html $tmpdir/times.txt
#vlc `./youtube-dl.py -g -f 18 http://www.youtube.com/watch?v=$id`

if [ -z "$other" ]
then
	mplayer -vo null `youtube-dl -g -f 18 http://www.youtube.com/watch?v=$id` 
else
	#mplayer `youtube-dl -g -f 18 http://www.youtube.com/watch?v=$id` -af pan=1:0.5:0.5	#Balance sound on both sides
	mplayer `youtube-dl -g -f 18 http://www.youtube.com/watch?v=$id` 
fi

#Fetch list of related videos
curl -q http://www.youtube.com/watch?v=$id -o $tmpdir/related.html
echo "Other related videos"
grep class=.title $tmpdir/related.html | cut -f6 -d'"' | nl

