import numpy as np
import os,sys, termios,tty

def getch(): 
	fd = sys.stdin.fileno()
	# save original terminal settings 
	old_settings = termios.tcgetattr(fd)
	 
	# change terminal settings to raw read
	tty.setraw(sys.stdin.fileno())
	ch = sys.stdin.read(1)
	# restore original terminal settings 
	termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		
	return ch


class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
		

mat=np.array(range(1,36)).reshape(5,7)
mat[-1,3:]=0

def print_matrix(mat,pos):
	p=0

	app="\t"*5
	print "\n"*3
	for nn in mat:
		print app,
		for mm in nn:
			if mm>0:
				if p==pos:
					print bcolors.FAIL+ '%02d' %mm + bcolors.ENDC+'\t',
				else:
					print '%02d\t' %mm,
			else:
				if p==pos:
					print bcolors.FAIL+ '__' + bcolors.ENDC+'\t',
				else:
					print "__"+'\t',
			p+=1
		print


def process_move(mat,pos,ch):
	y=pos/7
	x=pos%7

	if ch=='w':
		dx,dy=0,-1
	elif ch=='a':
		dx,dy=-1,0
	elif ch=='s':
		dx,dy=0,1
	elif ch=='d':
		dx,dy=1,0
	
	nx=(x+dx)%7
	ny=(y+dy)%5

	if mat[ny][nx]==0:
		mat[ny][nx],mat[y][x] = mat[y][x],mat[ny][nx]
		pos=(pos+dy*7+dx)%35
	else:
		print 'Illegal move'
	return pos
		

pos=0
os.system('clear')
print_matrix(mat,pos)

while True:
	ch=getch()

	if ch=='q':
		break

	if ch==']':
		pos=(pos+1)%35
	if ch==';':
		pos=(pos-1)%35
	if ch=='[':
		pos=(pos-7)%35
	if ch=="'":
		pos=(pos+7)%35


	# Define move commands here
	os.system('clear')
	if ch in 'wasd':
		pos=process_move(mat,pos,ch)

	print_matrix(mat,pos)

