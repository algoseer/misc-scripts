#!/usr/bin/python

import sys, tty, termios, os, time
from random import randint

def complete():

	count=[len(r) for r in rings]
	if count[1]==n or count[2]==n:
		return True
	else:
	
		return False	

def printRings(n,w):

	paddedRings=[[],[],[]]
	for k in xrange(3):
		paddedRings[k]=rings[k][:]
		paddedRings[k].extend(' '*(n-len(rings[k])))
		paddedRings[k].reverse()

	os.system('clear')	
	print "Number of moves=",moves
	
	print '\n'*5
	
	for i in xrange(n):
		print ('\t'.join([s[i] for s in paddedRings])).center(w,'\t')
	
	print '\t'.join(['_','_','_']).center(w,'\t')
	print '\t'.join(['1','2','3']).center(w,'\t')




def getch(): 
	fd = sys.stdin.fileno()
	# save original terminal settings 
	old_settings = termios.tcgetattr(fd)
	 
	# change terminal settings to raw read
	tty.setraw(sys.stdin.fileno())
	ch = sys.stdin.read(1)
	# restore original terminal settings 
	termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		
	return ch

def rand_init():
	r=[randint(0,2) for i in xrange(n)]
	
	for k in xrange(3):
		rings[k]=sorted([str(i+1) for i in xrange(n) if r[i]==k],key=int,reverse=True)
		
	
#Main program starts here

rings=[[],[],[]]

n=input('How many rings?')

#rings[0]=[str(a) for a in xrange(1,n+1)]
#rings[0].reverse()
rand_init()

key={}
moves=0
	
print('Input the keys for each tower')
print '1',
key[getch()]=1
print '2',
key[getch()]=2
print '3',
key[getch()]=3

while not complete():
	
	printRings(n,int(sys.argv[1]))
	fr=key[getch()]-1
	to=key[getch()]-1
	
	if len(rings[fr])>0 and ( len(rings[to])==0 or int(rings[to][-1])>=int(rings[fr][-1])):
		rings[to].append(rings[fr].pop())
		if fr!=to:
			moves+=1
	else:
		print "Invalid move"
		time.sleep(0.5)

printRings(n,int(sys.argv[1]))
print "Congrats!"
