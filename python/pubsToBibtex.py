import sys

#Script to parse a series of publications and change them to bibtex to be added easily by the database server
year='2014'
for pub in sys.stdin:
	pub=pub.rstrip().split('. ')
	authors=pub[0].split(', ')
	title=pub[1]
	conf= pub[2]

	author_bibstyle=[[b for b in a.split()] for a in authors]
	author_str=[];
	for b in author_bibstyle:
		author_str.append(' '.join(b[1:])+', '+b[0])


	print '@inproceedings{'+author_bibstyle[0][-1]+year+max(title.split(),key=len)+','
	print 'title= {'+title+'},'
	print 'author= {'+' and '.join(author_str)+'},'
	print 'booktitle= {'+conf[3:]+'},'
	print '}'

