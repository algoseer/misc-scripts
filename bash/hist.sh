#This script can prit a horizontal histogram of a stream of strings, Input can be piped in through stdin

sort | uniq -c | awk '!max{max=$1;}{r="";i=s=60*$1/max;while(i-->0)r=r"#";printf "%s %5d %s %s",$2,$1,r,"\n";}' <&0
