echo "Enter number of rings"
read rings

i=1
while [ $i -le $rings ]
do
	ring1[$i]=$i
	ring2[$i]=0
	ring3[$i]=0
	i=`expr $i + 1`
done

top1=1
top2=`expr $rings + 1`
top3=`expr $rings + 1`


moves=0
illegal=0
space="                            "
while true
do
	clear
	echo "No of moves:  "$moves
	echo
	echo
	echo
	echo
	echo
	echo
	echo
	echo
	echo

	i=1
	
	while [ $i -le $rings ]  
	do
		########## Print  a ring or space
		[[ ${ring1[i]} = 0 ]] && r1=" " || r1="${ring1[i]}"
		[[ ${ring2[i]} = 0 ]] && r2=" " || r2="${ring2[i]}"
		[[ ${ring3[i]} = 0 ]] && r3=" " || r3="${ring3[i]}"

		######################### edit this line to change the spacing	###########################
		echo $space" "$r1"          "$r2"           "$r3
		i=`expr $i + 1`
	done

	#check if the previous move was illegal

	if [ $illegal -eq 1 ]
	then
		echo "Illegal move"
	fi


	#check if the game is over
	i=1
	over=1
	while [ $i -le $rings ]
	do
		#echo ${ring2[i]} "-ne" $i "] && [ "${ring3[i]} "-ne" $i ]
		if [ ${ring2[i]} -ne $i ] && [ ${ring3[i]} -ne $i ]
			then
				over=0
		fi
		i=`expr $i + 1`
	done

	if [ $over -eq 1 ]
	then
		echo "Game Over! Congratulations!!"
		break
	fi
	
	read -s -n 1 source
	read -s -n 1 target	
	
	#check if move was illegal
	illegal=0
	case "$source" in

        1)      swap=${ring1[top1]}
		if [ $top1 -eq `expr $rings + 1`]		#there are no rings to take
		then
			illegal=1
		fi;;

        2)      swap=${ring2[top2]}
		if [ $top2 -eq `expr $rings + 1`]		#there are no rings to take
		then
			illegal=1
		fi;;

        3)      swap=${ring3[top3]}
		if [ $top3 -eq `expr $rings + 1`]		#there are no rings to take
		then
			illegal=1
		fi;;

        esac


	case "$target" in
	
	1)	if  [ $top1 -lt `expr $rings + 1` ] && [ ${ring1[top1]} -lt $swap ]
		then 
			illegal=1			
		fi;;
		
	
	2)	if   [ $top2 -lt `expr $rings + 1` ] && [ ${ring2[top2]} -lt $swap ]
		then 
			illegal=1
		fi;;

	3)	if  [ $top3 -lt `expr $rings + 1` ] && [ ${ring3[top3]} -lt $swap ]
		then 
			illegal=1
		fi;;
	esac

	if [ $illegal -eq 1 ]
	then
		continue
	else
		moves=`expr $moves + 1`			
	fi

	case "$source" in
	
	1)	ring1[$top1]=0
		top1=`expr $top1 + 1`;;

	2)	ring2[$top2]=0
		top2=`expr $top2 + 1`;;
		
	3)	ring3[$top3]=0
		top3=`expr $top3 + 1`;;
	
	esac
	
	illegal=0

	case "$target" in
	
	1)	ring1[`expr $top1 - 1`]=$swap
		top1=`expr $top1 - 1`;;
		
	
	2)	ring2[`expr $top2 - 1`]=$swap
		top2=`expr $top2 - 1`;;

	3)	ring3[`expr $top3 - 1`]=$swap
		top3=`expr $top3 - 1`;;
	esac
	
done
